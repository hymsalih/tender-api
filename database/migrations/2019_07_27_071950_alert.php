<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alert extends Migration
{
    public function up()
    {
        Schema::create('tbl_alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('memid', 34)->nullable();
            $table->string('item_id', 34);
            $table->string('section', 34);
            $table->string('f_date', 10);
            $table->string('f_time', 10);
            $table->string('status', 10);
            $table->string('x_days_before', 34);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_alerts');

    }
}
