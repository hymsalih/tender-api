<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mediatype', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('memid', 34)->nullable();
            $table->string('f_date', 10);
            $table->string('f_time', 10);
            $table->string('channels', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mediatype');

    }
}
