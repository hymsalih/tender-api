<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBookmarks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * - id
		- section (news, tender, reports, EventListing, Schedule, Speaker, Attendee )
		- itemID
		- datetime
		- status (active => ADD /inactive => REMOVE)
		- memid

         */
        Schema::create('tbl_bookmarks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('memid', 34)->nullable();
            $table->string('item_id', 34);
            $table->string('section', 34);
            $table->string('f_date', 10);
            $table->string('f_time', 10);
            $table->string('status', 10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bookmarks');
    }
}
