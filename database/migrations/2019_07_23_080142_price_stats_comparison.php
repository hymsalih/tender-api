<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PriceStatsComparison extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_price_stats_comparison', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('resource')->nullable();
            $table->string('comodityId_1', 34)->nullable();
            $table->string('regionId_1', 34)->nullable();
            $table->string('2ndlevel_1', 34)->nullable();
            $table->string('item_1_id', 34)->nullable();
            $table->string('item_2_id', 34)->nullable();
            $table->string('item_1_type', 34)->nullable();
            $table->string('item_2_type', 34)->nullable();
            $table->string('comodityId_2')->nullable();
            $table->string('regionId_2')->nullable();
            $table->string('2ndlevel_2')->nullable();
            $table->string('saved_on_time')->nullable();
            $table->string('memid')->nullable();
            $table->string('periodType')->nullable();
            $table->string('fromPeriod')->nullable();
            $table->string('toPeriod')->nullable();
            $table->string('comparisonName')->nullable();
            $table->string('datatype_1')->nullable();
            $table->string('datatype_2')->nullable();
            $table->string('relation_id_1')->nullable();
            $table->string('relation_id_2')->nullable();
            $table->string('source')->nullable();
            $table->string('status')->default('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_price_stats_comparison');

    }
}
