<?php

Route::post('/login', 'Api\AuthController@postLogin');
Route::post('/register', 'Api\AuthController@postRegister');
Route::post('/logout', 'Api\AuthController@postLogout');


Route::group(['prefix' => 'api', 'middleware' => 'apiToken'], function () {
    Route::post('/bookmark/add', 'Bookmark@add');
    Route::get('/bookmark/getBookmarkedItemIDs', 'Bookmark@get');
    Route::post('/bookmark/remove', 'Bookmark@remove');
    //
    Route::post('/comparison/save', 'Comparison@save');
    //
    Route::post('/prices/compare/savedlist', 'Price@savedlist');
    //
    Route::post('/alerts/add', 'Alert@add');
    Route::post('/alerts/remove', 'Alert@remove');
    Route::post('/alerts/getItemIDs', 'Alert@get');
    //
    Route::get('/alerts/mediatype', 'MediaType@get');
    Route::post('/alerts/mediatype', 'MediaType@save');
    //
    Route::post('/show/similar/save', 'ShowMoreLess@save');
    Route::post('/prices/compare/filter', 'Mapping@mapping');
});
Route::group(['prefix' => 'api'], function () {
//tender
    Route::get('/tenders/count', 'Tender@count');
    Route::get('/magazine', 'Magazine@get');
    Route::get('/reports', 'Report@get');
});



