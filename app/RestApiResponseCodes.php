<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestApiResponseCodes extends Model
{

    const  RequestSentSuccessfullyCode = 1001;
    const  RequestSentSuccessfullyMsg = 'Success';
    //
    const  InvalidInputsCode = 2001;
    const  InvalidInputsMsg = "Invalid Input";
    const  InvalidJsonRequest = "Invalid JSON format";
    //
    const  InvalidOTPCode = 2002;
    const  InvalidOTPCodeMsg = "Invalid OTP";
    //
    const  profileNotCompleteCode = 2005;
    const  profileNotCompleteMsg = "Profile not complete";
    //
    const  userNotFoundCode = 2013;
    const  userNotFoundMsg = 'User not found';
    const  recNotFoundMsg = 'No records found to update';
    //
    const  mandatoryParamsMissingCode = 2014;
    const  mandatoryParamsMissingMsg = 'Mandatory parameters missing';
    //
    const  accessTokenWasExpiredOrInvalidCode = 2100;
    const  accessTokenWasExpiredOrInvalidMsg = "Access token or memId is invalid or has expired";
    const  accessResourceTypeRequiredMsg = "Resource type cannot be empty";
    const  accessResourceTypeInvalidMsg = "Invalid resource type";
    //
    const  somethingWentWrongCode = 9999;
    const  somethingWentWrongMsg = "Something went wrong";


    //
    const  somethingDataAlreadyExistsCode = 10003;
    const  somethingDataAlreadyExistsMsg = "Data already exists";
}
