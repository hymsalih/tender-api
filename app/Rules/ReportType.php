<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ReportType implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {            return true;

        if (in_array($value, ['special', 'premium', 'regular'])) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute must should be one of [special, premium, regular]';
    }
}
