<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bodyContent = $request->getContent();
        if (!\App\Helper::isJSON($bodyContent)) {
            return \App\Helper::invalidJsonResponse();
        }
        $data = json_decode($bodyContent, true);
        if (empty($data['resourceType'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::accessTokenWasExpiredOrInvalidCode,
                    'message' => \App\RestApiResponseCodes::accessResourceTypeRequiredMsg
                ]
            ], 400);
        } else if (!empty($data['resourceType']) && empty($data['accessToken']) && empty($data['memId'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::accessTokenWasExpiredOrInvalidCode,
                    'message' => \App\RestApiResponseCodes::accessTokenWasExpiredOrInvalidMsg
                ]
            ], 400);
        } else if (!empty($data['resourceType']) && !in_array($data['resourceType'], ['app', 'web'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::accessTokenWasExpiredOrInvalidCode,
                    'message' => \App\RestApiResponseCodes::accessResourceTypeInvalidMsg
                ]
            ], 400);
        } else if (!empty($data['resourceType']) && $data['resourceType'] == 'app' && isset($data['memId'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::InvalidInputsCode,
                    'message' => "You cannot use both app resource and memId"
                ]
            ], 400);
        } else if (!empty($data['resourceType']) && $data['resourceType'] == 'web' && isset($data['accessToken'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::InvalidInputsCode,
                    'message' => "You cannot use both web resource and accessToken"
                ]
            ], 400);
        } else if (!empty($data['accessToken']) && !empty($data['memId'])) {
            $hasToken = \App\Token::select('*')->where('token', $data['accessToken'])->where('id', $data['memId'])->first();
            if (empty($hasToken)) {
                return \App\Helper::invalidAuthentication();
            }
        } else if (empty($data['accessToken']) || !empty($data['memId'])) {
            $hasToken = \App\Token::select('*')->where('id', $data['memId'])->first();
            if (empty($hasToken)) {
                return \App\Helper::invalidAuthentication();
            }
        } else if (!empty($data['accessToken']) || empty($data['memId'])) {
            $hasToken = \App\Token::select('*')->where('token', $data['accessToken'])->first();
            if (empty($hasToken)) {
                return \App\Helper::invalidAuthentication();
            }
        } else if (empty($data['accessToken']) || empty($data['memId'])) {
            return \App\Helper::invalidAuthentication();
        }
        session()->put(['resourceType' => $data['resourceType'], 'memId' => $hasToken->id, 'token' => $hasToken->token]);
        return $next($request);
    }
}
