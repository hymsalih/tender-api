<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;


class MediaType extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }

    public function save(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'channels' => 'required',
            'memId' => 'int',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $channels = implode(", ", $data['channels']);
        $hasChannel = \App\MediaType::select('*')->where('memid', $memId)->where('channels', $channels)->first();
        if (!$hasChannel) {
            \App\MediaType::insert([
                'memid' => $memId,
                'f_date' => date("Y-m-d"),
                'f_time' => date("h:i:s"),
                'channels' => $channels,
            ]);
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg
                ]
            ]);
        } else {
            \App\MediaType::where('id', $hasChannel->id)->update([
                'f_date' => date("Y-m-d"),
                'f_time' => date("h:i:s"),
                'channels' => $channels,
            ]);

            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg
                ]
            ]);
        }

    }

    public function get(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $channels = \App\MediaType::select('channels')->where('memid', $memId)->pluck('channels')->first();
        $channels = (!empty($channels) ? explode(",", $channels) : []);
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => ["channels" => $channels]
            ]
        ]);
    }


}
