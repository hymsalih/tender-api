<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use DB;
use Illuminate\Http\Request;
use Validator;

class AuthController extends Controller
{
    private $apiToken;

    public function __construct()
    {
        // Unique Token
        $this->apiToken = uniqid(base64_encode(str_random(60)));
    }

    /**
     * Client Login
     */
    public function postLogin(Request $request)
    {
        // Validations
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $validator->messages(),
                ]
            ]);

        } else {
            // Fetch User
            $user = User::where('email', $request->email)->first();
            if ($user) {
                // Verify the password
                if (password_verify($request->password, $user->password)) {
                    // Update Token
                    $postArray = ['token' => $this->apiToken];
                    $login = \App\Token::where('id', $user->id)->update($postArray);
                    if ($login) {
                        return response()->json([
                            'status' => [
                                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                                'data' => [
                                    'memid' => $user->id,
                                    'name' => $user->name,
                                    'email' => $user->email,
                                    'access_token' => $this->apiToken,
                                ]
                            ]
                        ]);
                    }
                } else {
                    return response()->json([
                        'status' => [
                            "code" => \App\RestApiResponseCodes::InvalidInputsCode,
                            'message' => \App\RestApiResponseCodes::InvalidInputsMsg
                        ]
                    ]);
                }
            } else {
                return response()->json([
                    'status' => [
                        "code" => \App\RestApiResponseCodes::userNotFoundCode,
                        'message' => \App\RestApiResponseCodes::userNotFoundMsg
                    ]
                ]);
            }
        }
    }

    /**
     * Register
     */
    public function postRegister(Request $request)
    {
        // Validations
        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|unique:tbl_users,email',
            'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $validator->messages(),
                ]
            ]);
        } else {
            $postArray = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'created_at' => date("Y-m-d h:i:s"),
                'updated_at' => date("Y-m-d h:i:s"),
            ];
            // $user = User::GetInsertId($postArray);
            $user = User::create($postArray);
            if ($user) {
                \App\Token::insert(['id' => $user->id, 'token' => $this->apiToken]);
                return response()->json([
                    'status' => [
                        "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                        'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                        'data' => [
                            'memid' => $user->id,
                            'name' => $request->name,
                            'email' => $request->email,
                            'access_token' => $this->apiToken,
                        ]
                    ]
                ]);
            } else {
                return response()->json([
                    'status' => [
                        "code" => \App\RestApiResponseCodes::somethingWentWrongCode,
                        'message' => \App\RestApiResponseCodes::somethingWentWrongMsg
                    ]
                ]);
            }
        }
    }

    public function postLogout(Request $request)
    {
        $token = $request->input('accessToken');
        $user = \App\Token::where('token', $token)->first();
        if ($user) {
            $postArray = ['token' => null];
            $logout = \App\Token::where('id', $user->id)->update($postArray);
            if ($logout) {
                return response()->json([
                    'message' => 'User Logged Out',
                ]);
            }
        } else {
            return response()->json([
                'message' => 'User not found',
            ]);
        }
    }
}
