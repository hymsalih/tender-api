<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Validator;

class Magazine extends Controller
{
    public function get(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $auth = 0;

        $validator = Validator::make($data, [
            'pageNumber' => 'required|int',
//            'filter.month' => ['required', new MonthValidation()],
//            'filter.year' => ['required', new YearValidation()],
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $hasToken = 0;
        if (isset($data['memId']) || isset($data['accessToken'])) {
            if (!empty($data['accessToken']) && !empty($data['memId'])) {
                $hasToken = \App\Token::select('*')->where('token', $data['accessToken'])->where('id', $data['memId'])->first();
                if (empty($hasToken)) {
                    return \App\Helper::invalidAuthentication();
                }
            } else if (empty($data['accessToken']) || !empty($data['memId'])) {
                $hasToken = \App\Token::select('*')->where('id', $data['memId'])->first();
                if (empty($hasToken)) {
                    return \App\Helper::invalidAuthentication();
                }
            } else if (!empty($data['accessToken']) || empty($data['memId'])) {
                $hasToken = \App\Token::select('*')->where('token', $data['accessToken'])->first();
                if (empty($hasToken)) {
                    return \App\Helper::invalidAuthentication();
                }
            }

        }
        $auth = (!empty($hasToken) ? 1 : 0);
        $filters = ['m','y'];
        $m = $year = "";
        if (isset($data['filter'])) {
            if (isset($data['filter']['month'])) {
                $m = date("m", strtotime($data['filter']['month']));
            }
            if (isset($data['filter']['year'])) {
                $year = $data['filter']['year'];
            }
        }
        $page = $data['pageNumber'];
        $limit = 4;
        $magazines = DB::table('tbl_magazine_issue')
            ->where(function ($query) use ($m,$year) {
                if(!empty($m)){
                    $query->whereMonth('issue_date', $m);
                }
                if(!empty($year)){
                    $query->whereYear('issue_date', $year);
                }
            })
            ->orderBy('id', 'desc')
            ->limit($limit)
            ->offset(($page - 1) * $limit)
            ->get();
        $rMagazines = [];
        foreach ($magazines as $key => $magazine) {
            if ($auth) {
                $bookmarked = \App\Bookmark::select('*')->where('item_id', $magazine->id)->where('memid', $data['memId'])->where('section', 'steel360')->count();
            } else {
//                $bookmarked = \App\Bookmark::select('*')->where('item_id', $magazine->id)->where('section', 'steel360')->count();
                $bookmarked = 0;
            }
            $rMagazines[] = array(
                "magazineId" => $magazine->id,
                "magazinePreviewURL" => $magazine->preview_link,
                "magazineDownloadURL" => ($auth == 0 ? "hidden" : $magazine->download_link),
                "thumbImageURL" => $magazine->thumb_image,
                "imageURL" => $magazine->image,
                "isBookmarked" => ($auth > 0 ? ($bookmarked > 0 ? "Y" : "N") : "N"),
                "title" => $magazine->title,
                "datePublished" => $magazine->issue_date
            );
        }
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => $rMagazines
            ]
        ]);
    }
}
