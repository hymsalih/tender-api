<?php

namespace App\Http\Controllers;

use DB;

class Tender extends Controller
{
    public function count2()
    {
        $c = array();
        $a = array();
        $titles = DB::table('tbl_tenders')->select('buy_sell')->groupBy('buy_sell')->get();
        foreach ($titles as $aKey => $title) {
            $categories = DB::select(DB::raw("select commodityID, count(id) as cnt from tbl_tenders where buy_sell = '" . $title->buy_sell . "' group by commodityID"));
            foreach ($categories as $category) {
                $c[$title->buy_sell][] = [
                    'id' => $category->commodityID,
                    'count' => $category->cnt
                ];
                $a[] = [
                    'id' => $category->commodityID,
                    'count' => $category->cnt
                ];
            }
        }
        $c['all'] = $a;
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => $c
            ]
        ]);
    }
    public function count()
    {
        $c = array();
        $a = array();
        $titles = DB::table('tbl_tenders')->select('buy_sell')->groupBy('buy_sell')->get();
        foreach ($titles as $aKey => $title) {
            $categories = DB::select(DB::raw("select commodityID, count(id) as cnt from tbl_tenders where buy_sell = '" . $title->buy_sell . "' group by commodityID"));
            foreach ($categories as $category) {
                $subCategories = DB::select(DB::raw("select subCommodityID, count(id) as cnt from tbl_tenders where buy_sell = '" . $title->buy_sell . "' and commodityID = '" . $category->commodityID . "' group by subCommodityID"));
                $c[$title->buy_sell][] = [
                    'id' => $category->commodityID,
                    'count' => $subCategories[0]->cnt
                ];
                $a[] = [
                    'id' => $category->commodityID,
                    'count' => $subCategories[0]->cnt
                ];
            }
        }
        $c['all'] = $a;
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => $c
            ]
        ]);
    }
}
