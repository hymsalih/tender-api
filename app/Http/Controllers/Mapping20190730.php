<?php

namespace App\Http\Controllers;

use App\Rules\TradeTypes;
use DB;
use Illuminate\Http\Request;
use Validator;

class Mapping extends Controller
{
    public function mapping(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'regionID' => 'int',
            'tradeType' => ['required', new TradeTypes],
            'section' => 'required',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }

        if ($data['section'] == "prices") {
            if ($data['tradeType'] == "domestics") {
                //tbl_dom_master
                $domestics = DB::table('tbl_dom_master')
                    ->select('*')
                    ->where('category', 'Iron Ore')
                    ->groupBy('category')
                    ->get();
                $nArr = [];
                foreach ($domestics as $key => $domestic) {
                    $nArr[$key] = array(
                        "id" => $domestic->id,
                        "type" => "Commodity",
                        "value" => $domestic->category,
                        "1Level" => []
                    );
                    $subCategories1 = DB::table('tbl_dom_master')
                        ->where('category', $domestic->category)
                        ->groupBy('sub_category')
                        ->get();
                    foreach ($subCategories1 as $key1 => $catSub) {
                        $nArr[$key]["1Level"][$key1] = array(
                            "id" => $catSub->id,
                            "value" => $catSub->sub_category,
                            "2Level" => [],
                        );
                        $subCategories2 = DB::table('tbl_dom_master')
                            ->select('state', 'id')
                            ->where('category', $domestic->category)
                            ->where('sub_category', $catSub->sub_category)
                            ->groupBy('state')
                            ->get();
                        $stateArray = [];
                        foreach ($subCategories2 as $key2 => $subCat2) {
                            if ($domestic->category == "Iron Ore" && $catSub->sub_category == "Fines/Lumps" || $catSub->sub_category == "FINES/LUMPS") {
                                $stateArray[$key2] = array(
                                    "value" => $subCat2->state
                                );
                                $subCategories3 = DB::table('tbl_dom_master')
                                    ->select('state')
                                    ->where('state', $subCat2->state)
                                    ->where('category', $domestic->category)
                                    ->where('sub_category', $catSub->sub_category)
                                    ->groupBy('state')
                                    ->get();
                                $suppliers = [];
                                foreach ($subCategories3 as $key3 => $subCat3) {
                                    $subCategories4 = DB::table('tbl_dom_master')
                                        ->select('f_grade', 'f_size', 'id')
                                        ->where('state', $subCat2->state)
                                        ->where('category', $domestic->category)
                                        ->where('sub_category', $catSub->sub_category)
                                        ->where('supplier', $catSub->supplier)
                                        ->get();
                                    $grad_size = [];
                                    foreach ($subCategories4 as $subCat4) {
                                        $grad_size[] = [
                                            'id' => $subCat4->id,
                                            'value' => $subCat4->f_size . " , " . $subCat4->f_grade,
                                        ];
                                    }
                                    $suppliers[$key3] = array(
                                        "value" => $subCat3->supplier,
                                        "4Level" => $grad_size
                                    );
                                }
                                $stateArray[$key2]['3Level'] = $suppliers;
                            } else {
                                $stateArray[$key2] = array(
                                    "id" => $subCat2->id,
                                    "value" => $subCat2->state
                                );
                            }
                        }
                        $nArr[$key]["1Level"][$key1]['2Level'] = $stateArray;
                    }
                }
                echo json_encode($nArr);
            } else {
                //tbl_exim_master
                if ($data['tradeType'] == "import") {
                } //                    consider rows having unload_countryID = regionID
                else {
//                        consider rows having load_countryID = regionID
                }
            }

        }
    }
}
