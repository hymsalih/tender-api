<?php

namespace App\Http\Controllers;

use App\Rules\ShowLessType;
use Illuminate\Http\Request;
use Validator;

class ShowMoreLess extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }

    public function save(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'memId' => 'int',
            'itemId' => 'required',
            'type' => ['required', new ShowLessType],
            'section' => 'required',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $hasRec = \App\ShowMoreLess::select('*')->where('memid', $memId)
            ->where('item_id', $data['itemId'])
            ->where('section', $data['section'])
            ->first();
        if (empty($hasRec)) {
            \App\ShowMoreLess::insert([
                'memid' => $memId,
                'section' => $data['section'],
                'item_id' => $data['itemId'],
                'action' => $data['type'],
                'f_date' => date("Y-m-d"),
                'f_time' => date("h:i:s"),
            ]);
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => 'successfully added to portfolio'
                ]
            ]);
        }
        \App\ShowMoreLess::where('id', $hasRec->id)->update([
            'action' => $data['type'],
        ]);
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => 'successfully updated to portfolio'
            ]
        ]);

    }
}
