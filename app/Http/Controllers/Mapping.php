<?php

namespace App\Http\Controllers;

use App\Rules\TradeTypes;
use DB;
use Illuminate\Http\Request;
use Validator;

class Mapping extends Controller
{
    public function mapping(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'regionID' => 'int',
            'tradeType' => ['required', new TradeTypes],
            'section' => 'required',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }

        if ($data['section'] == "prices") {
            if ($data['tradeType'] == "domestics") {
                //tbl_dom_master
                $domestics = DB::table('tbl_dom_master')
                    ->select('*')
                    ->where('category', 'Iron Ore')
                    ->groupBy('category')
                    ->get();
                $domesticArray = [];
                foreach ($domestics as $key => $domestic) {
//                    echo $domestic->category . "\n";
                    $category_one = DB::table('tbl_dom_master')
                        ->where('category', $domestic->category)
                        ->groupBy('sub_category')
                        ->get();
                    $categoryArray = [];
                    foreach ($category_one as $key1 => $catSubOne) {
                        $states = DB::table('tbl_dom_master')
                            ->select('state', 'id')
                            ->where('category', $domestic->category)
                            ->where('sub_category', $catSubOne->sub_category)
                            ->groupBy('state')
                            ->get();
                        $statesArray = [];
                        foreach ($states as $key2 => $state) {
                            $suppliers = $subCategories2 = DB::table('tbl_dom_master')
                                ->select('*')
                                ->where('category', $domestic->category)
                                ->where('sub_category', $catSubOne->sub_category)
                                ->where('state', $state->state)
                                ->groupBy('sub_category')
                                ->get();
//                            echo "---" . $state->state . "\n";
                            $suppliersArray = [];
                            foreach ($suppliers as $supplier) {
//                                echo "------" . $supplier->supplier . "\n";
                                $grades = DB::table('tbl_dom_master')
                                    ->select('f_grade', 'f_size', 'id')
                                    ->where('state', $state->state)
                                    ->where('category', $domestic->category)
                                    ->where('sub_category', $catSubOne->sub_category)
                                    ->where('supplier', $supplier->supplier)
                                    ->get();
                                $gradesArray = [];
                                foreach ($grades as $grade) {
                                    $gradesArray[] = ['id' => $grade->id, 'value' => $grade->f_size . " , " . $grade->f_grade];
//                                    echo "---------" . $grade->f_size . " , " . $grade->f_grade . "\n";
                                }
                                $suppliersArray[] = [
                                    "value" => $supplier->supplier,
                                    "4Level" => $gradesArray
                                ];
                            }
                            $statesArray[] = [
                                "value" => $state->state,
                                "3Level" => $suppliersArray
                            ];

                        }
                        $categoryArray[] = [
                            'id' => $catSubOne->id,
                            'value' => $catSubOne->sub_category,
                            '2Level' => $statesArray,
                        ];
                    }
                    $domesticArray[] = [
                        'id' => $domestic->id,
                        'value' => $domestic->category,
                        '1Level' => $categoryArray
                    ];
                }

                $domestics = DB::table('tbl_dom_master')
                    ->select('*')
                    ->groupBy('category')
                    ->get();
                foreach ($domestics as $key => $domestic) {
                    $category_one = DB::table('tbl_dom_master')
                        ->where('category', $domestic->category)
                        ->groupBy('sub_category')
                        ->get();
                    $categoryArray = [];
                    foreach ($category_one as $key1 => $catSubOne) {
                        $grades = DB::table('tbl_dom_master')
                            ->select('f_grade', 'f_size', 'id')
                            ->where('category', $domestic->category)
                            ->where('sub_category', $catSubOne->sub_category)
                            ->get();
                        $gradesArray = [];
                        foreach ($grades as $grade) {
                            $gradesArray[] = ['id' => $grade->id, 'value' => $grade->f_size . " , " . $grade->f_grade];
                        }
                        $categoryArray[] = [
                            'id' => $catSubOne->id,
                            'value' => $catSubOne->sub_category,
                            '1Leve2' => $gradesArray,
                        ];
                    }
                    $domesticArray[] = [
                        'id' => $domestic->id,
                        'value' => $domestic->category,
                        '1Level' => $categoryArray
                    ];
                }
                echo json_encode($domesticArray);
            } else {
                //tbl_exim_master
                if ($data['tradeType'] == "import") {
                } //                    consider rows having unload_countryID = regionID
                else {
//                        consider rows having load_countryID = regionID
                }
            }

        }
    }


    public function mapping01(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'regionID' => 'int',
            'tradeType' => ['required', new TradeTypes],
            'section' => 'required',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }

        if ($data['section'] == "prices") {
            if ($data['tradeType'] == "domestics") {
                //tbl_dom_master
                $domestics = DB::table('tbl_dom_master')
                    ->select('*')
//                    ->where('category', 'Iron Ore')
                    ->groupBy('category')
                    ->get();
                $domesticArray = [];
                foreach ($domestics as $key => $domestic) {
//                    echo $domestic->category . "\n";
                    $category_one = DB::table('tbl_dom_master')
                        ->where('category', $domestic->category)
                        ->groupBy('sub_category')
                        ->get();
                    $categoryArray = [];
                    foreach ($category_one as $key1 => $catSubOne) {
//                        echo $catSubOne->sub_category . "\n";
                        $states = DB::table('tbl_dom_master')
                            ->select('state', 'id')
                            ->where('category', $domestic->category)
                            ->where('sub_category', $catSubOne->sub_category)
                            ->groupBy('state')
                            ->get();
                        $statesArray = [];
                        foreach ($states as $key2 => $state) {
                            $suppliers = $subCategories2 = DB::table('tbl_dom_master')
                                ->select('*')
                                ->where('category', $domestic->category)
                                ->where('sub_category', $catSubOne->sub_category)
                                ->where('state', $state->state)
                                ->groupBy('sub_category')
                                ->get();
//                            echo "---" . $state->state . "\n";
                            $suppliersArray = [];
                            foreach ($suppliers as $supplier) {
//                                echo "------" . $supplier->supplier . "\n";
                                $grades = DB::table('tbl_dom_master')
                                    ->select('f_grade', 'f_size', 'id')
                                    ->where('state', $state->state)
                                    ->where('category', $domestic->category)
                                    ->where('sub_category', $catSubOne->sub_category)
                                    ->where('supplier', $supplier->supplier)
                                    ->get();
                                $gradesArray = [];
                                foreach ($grades as $grade) {
                                    $gradesArray[] = ['id' => $grade->id, 'value' => $grade->f_size . " , " . $grade->f_grade];
//                                    echo "---------" . $grade->f_size . " , " . $grade->f_grade . "\n";
                                }
                                $suppliersArray[] = [
                                    "value" => $supplier->supplier,
                                    "4Level" => $gradesArray
                                ];
                            }
                            $statesArray[] = [
                                "value" => $state->state,
                                "3Level" => $suppliersArray
                            ];
                        }
                        $categoryArray[] = [
                            'id' => $catSubOne->id,
                            'value' => $catSubOne->sub_category,
                            '2Level' => $statesArray,
                        ];
                    }
                    $domesticArray[] = [
                        'id' => $domestic->id,
                        'value' => $domestic->category,
                        '1Level' => $categoryArray
                    ];
                }
                echo json_encode($domesticArray);
            } else {
                //tbl_exim_master
                if ($data['tradeType'] == "import") {
                } //                    consider rows having unload_countryID = regionID
                else {
//                        consider rows having load_countryID = regionID
                }
            }

        }
    }
}
