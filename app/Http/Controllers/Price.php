<?php

namespace App\Http\Controllers;

use App\RestApiResponseCodes;
use Illuminate\Http\Request;
use Validator;

class Price extends Controller
{
    public function savedlist(Request $request)
    {
        $nData = [];
        $memId = \App\Helper::getMemId();
        $resourceType = \App\Helper::getResourceType();
        $data = json_decode($request->getContent(), true);

        if ($resourceType == 'app') {
            $data = \App\Comparision::select('*')
                ->where('memid', $memId)
                ->where('resource', 'app')
                ->orderBy('id', 'desc')->get();
            foreach ($data as $d) {
                $nData[] = array(
                    "itemId" => $d->id,
                    "comparisonName" => $d->comparisonName,
                    "fromDate" => $d->fromPeriod,
                    "toDate" => $d->toPeriod,
                    "assessmentType" => $d->periodType,
                    "item1" => array(
                        "id" => $d->item_1_id,
                        "legend" => "",
                    ),
                    "item2" => array(
                        "id" => $d->item_2_id,
                        "legend" => "",
                    )
                );
            }
        } else {
            $data = \App\Comparision::select('*')
                ->where('memid', $data['memId'])->orderBy('id', 'desc')
                ->where('resource', 'web')
                ->get()
                ->toArray();
            foreach ($data as $d) {
                $nData[] = array(
                    "itemId" => $d['id'],
                    "comparisonName" => $d['comparisonName'],
                    "fromDate" => $d['fromPeriod'],
                    "toDate" => $d['toPeriod'],
                    "assessmentType" => $d['periodType'],
                    "item1" => array(
                        "dataType" => $d['datatype_1'],
                        "comodityId" => $d['comodityId_1'],
                        "regionId" => $d['regionId_1'],
                        "relationId" => $d['relation_id_1'],
                        "2ndlevel" => $d['2ndlevel_1'],
                    ),
                    "item2" => array(
                        "dataType" => $d['datatype_2'],
                        "comodityId" => $d['comodityId_2'],
                        "regionId" => $d['regionId_2'],
                        "relationId" => $d['relation_id_2'],
                        "2ndlevel" => $d['2ndlevel_2'],
                    )
                );
            }
        }
        return response()->json([
            'status' => [
                "code" => RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => (!empty($nData) ? $nData : [])
            ]
        ]);
    }
}
