<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

class Alert extends Controller
{
    public function __construct()
    {
        $this->middleware('api');

    }

    public function get(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'section' => 'required',
            'memId' => 'int',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $alertIds = \App\Alert::select('item_id')
            ->where('section', $data['section'])
            ->where('memid', $memId)
            ->where('status', 'active')
            ->pluck('item_id');
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => (!empty($alertIds) ? $alertIds : [])
            ]
        ]);
    }

    public function remove(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'section' => 'required',
            'memId' => 'int',
            'itemId' => 'required'
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $r = \App\Alert::where('item_id', $data['itemId'])
            ->where('section', $data['section'])
            ->where('status', 'active')
            ->where('memid', $memId)
            ->first();
        if (!empty($r)) {
            \App\Alert::where('id', $r->id)
                ->update(['status' => 'inactive']);
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg
                ]
            ]);

        } else {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => "Record already deleted or no record found"
                ]
            ]);
        }
    }

    public function add(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'section' => 'required',
            'itemId' => 'required',
            'memId' => 'int',
            'xDaysBefore' => 'int',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }

        if (!isset($data['xDaysBefore'])) {
            $data['xDaysBefore'] = "";
        }
        if ($data['section'] == 'tender' && empty($data['xDaysBefore'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::InvalidInputsCode,
                    'message' => 'xDaysBefore only required for section tender'
                ]
            ]);

        } else if ($data['section'] != 'tender' && !empty($data['xDaysBefore'])) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::InvalidInputsCode,
                    'message' => 'xDaysBefore only required for section tender'
                ]
            ]);
        }
        $f = \App\Alert::where('item_id', $data['itemId'])
            ->where('section', $data['section'])
            ->where('memid', $memId)
            ->where('status', 'active')
            ->count();
        if ($f) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => 'Alert already exists'
                ]
            ]);
        }
        \App\Alert::insert([
            'memid' => $memId,
            'section' => $data['section'],
            'item_id' => $data['itemId'],
            'status' => 'active',
            'f_date' => date("Y-m-d"),
            'f_time' => date("h:i:s"),
            'x_days_before' => $data['xDaysBefore'],
        ]);
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => 'Successfully added to alert'
            ]
        ]);
    }
}
