<?php

namespace App\Http\Controllers;

use App\Rules\Source;
use Illuminate\Http\Request;
use Session;
use Validator;
use DB;

class Bookmark extends Controller
{

    public function __construct()
    {
        $this->middleware('api');

    }


    public function get(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'section' => 'required',
            'memId' => 'int',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $bookmarkItemIds = \App\Bookmark::select('item_id')
            ->where('section', $data['section'])
            ->where('memid', $memId)
            ->where('status', 'active')
            ->pluck('item_id');
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg,
                'data' => (!empty($bookmarkItemIds) ? $bookmarkItemIds : [])
            ]
        ]);
    }

    public function remove(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'memId' => 'int',
            'itemIds' => 'required',
            'source' => ['required', new Source],

        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $tblArray = \App\Helper::tblLists();

        try {
            if (array_key_exists($data['source'], $tblArray)) {
                $r = DB::table($tblArray[$data['source']])
                    ->where('id', $data['itemIds'])
                    ->where('status', 'active')
                    ->where('memid', $memId)
                    ->first();
                if (!empty($r)) {
                    \App\Bookmark::where('id', $r->id)
                        ->update(['status' => 'inactive']);
                    return response()->json([
                        'status' => [
                            "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                            'message' => \App\RestApiResponseCodes::RequestSentSuccessfullyMsg
                        ]
                    ]);
                } else {
                    return response()->json([
                        'status' => [
                            "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                            'message' => "Record already deleted or no record found"
                        ]
                    ]);
                }
            } else {
                return response()->json([
                    'status' => [
                        "code" => \App\RestApiResponseCodes::somethingWentWrongCode,
                        'message' => \App\RestApiResponseCodes::somethingWentWrongMsg,
                    ]
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::somethingWentWrongCode,
                    'message' => \App\RestApiResponseCodes::somethingWentWrongMsg,
                ]
            ]);
        }

    }

    public function add(Request $request)
    {
        $memId = \App\Helper::getMemId();
        $data = json_decode($request->getContent(), true);
        $validator = Validator::make($data, [
            'section' => 'required',
            'itemIds' => 'required',
            'memId' => 'int',
        ]);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::mandatoryParamsMissingCode,
                    'message' => $messages
                ]
            ]);
        }
        $f = \App\Bookmark::where('item_id', $data['itemIds'])
            ->where('section', $data['section'])
            ->where('memid', $memId)
            ->where('status', 'active')
            ->count();
        if ($f) {
            return response()->json([
                'status' => [
                    "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                    'message' => 'Bookmark already exists'
                ]
            ]);
        }
        \App\Bookmark::insert([
            'memid' => $memId,
            'section' => $data['section'],
            'item_id' => $data['itemIds'],
            'event_id' => (isset($data['eventID'])?$data['eventID']:''),
            'status' => 'active',
            'f_date' => date("Y-m-d"),
            'f_time' => date("h:i:s"),
        ]);
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::RequestSentSuccessfullyCode,
                'message' => 'Successfully added to bookmarks'
            ]
        ]);
    }
}
