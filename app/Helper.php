<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

class Helper extends Model
{
    public static function tblLists()
    {
        return ['bookmark' => 'tbl_bookmarks', 'compare' => 'tbl_price_stats_comparison', 'alerts' => 'tbl_alerts'];
    }

    public static function sectionArray()
    {
        return ["News", "Tender", "Reports", "EventListing", "Schedule", "Speaker", "Attendee"];
    }

    public static function invalidJsonResponse()
    {
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::InvalidInputsCode,
                'message' => \App\RestApiResponseCodes::InvalidJsonRequest
            ]], 400);
    }

    public static function invalidAuthentication()
    {
        return response()->json([
            'status' => [
                "code" => \App\RestApiResponseCodes::accessTokenWasExpiredOrInvalidCode,
                'message' => \App\RestApiResponseCodes::accessTokenWasExpiredOrInvalidMsg
            ]], 400);
    }

    public static function getMemId()
    {
        return session()->get('memId');
    }

    public static function getResourceType()
    {
        return session()->get('resourceType');
    }

    public static function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }


    public static function validation($data)
    {
        if (!empty($data['accessToken']) && !empty($data['memid'])) {
            return [
                'status' => [
                    "code" => RestApiResponseCodes::InvalidInputsCode,
                    'message' => RestApiResponseCodes::InvalidInputsMsg
                ]
            ];
        }
        if (empty($data['accessToken']) && empty($data['memId'])) {
            return [
                'status' => [
                    "code" => RestApiResponseCodes::InvalidInputsCode,
                    'message' => RestApiResponseCodes::InvalidInputsMsg
                ]
            ];
        }

        if (isset($data['resourceType']) && $data['resourceType'] == "app" && isset($data['memId'])) {
            return [
                'status' => [
                    "code" => RestApiResponseCodes::InvalidInputsCode,
                    'message' => RestApiResponseCodes::InvalidInputsMsg
                ]
            ];
        }


        if (isset($data['resourceType']) && $data['resourceType'] == "web" && isset($data['accessToken'])) {
            return [
                'status' => [
                    "code" => RestApiResponseCodes::InvalidInputsCode,
                    'message' => RestApiResponseCodes::InvalidInputsMsg
                ]
            ];
        }


        if (isset($data['accessToken']) && isset($data['memId'])) {
            return [
                'status' => [
                    "code" => RestApiResponseCodes::InvalidInputsCode,
                    'message' => RestApiResponseCodes::InvalidInputsMsg
                ]
            ];
        }


        $user_id = null;
        if (!empty($data['accessToken'])) {
            $e = \App\Helper::validateAccessToken($data['accessToken']);
            if (!$e['user_id']) {
                return [
                    'status' => [
                        "code" => RestApiResponseCodes::accessTokenWasExpiredOrInvalidCode,
                        'message' => RestApiResponseCodes::accessTokenWasExpiredOrInvalidMsg
                    ]
                ];
            }
            $user_id = $e['user_id'];
        }
        return [
            'status' => [
                'message' => 'success',
                'user_id' => $user_id,
            ]
        ];

    }

}
