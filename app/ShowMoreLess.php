<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowMoreLess extends Model
{
    protected $table = 'tbl_show_more_less_like';
    public $timestamps = false;
}
