<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comparision extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_price_stats_comparison';
}
